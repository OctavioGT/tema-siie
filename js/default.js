﻿// Octavio Gutiérrez | lacho.gutíerrez@gmail.com
// SIIE 16-22 v 0.1
// jQuery/JavaScript

$(document).ready(function ($) {

    //Fx Scrolling
    $(function () {
        $('#menuLat a').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({ scrollTop: $($anchor.attr('href')).offset().top }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });

     
    // Fix Menu
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200 && $('#menu-open').is(':checked')==false) {
            $("#menuLat").addClass("xFix");
            $(".logomini").show();           
        } else {
            $("#menuLat").removeClass("xFix");
            $(".logomini").hide();            
        }
    });

    // Trigger al activar sección
    $('#menuLat').on('activate.bs.scrollspy', function () {
        var hash = $(this).find("li.active a").attr("href");
        SeccionActive(hash);
    });

    // Moz Over
    $(".afx1").mouseenter(function (e) { $(this).children("div.over").stop(true, true).fadeIn(350) });
    $(".afx1").mouseleave(function (e) { $(this).children("div.over").stop(true, true).fadeOut(600) });

    
    $(".afx2").mouseenter(function (e) { $(this).children("div.over").stop(true, true).show('slide', { direction: 'left' }); });
    $(".afx2").mouseleave(function (e) { $(this).children("div.over").stop(true, true).hide('slide', { direction: 'left' }, 'slow');});

    $(".afx3").mouseenter(function (e) { $(this).children("div.over").stop(true, true).fadeIn(350) });
    $(".afx3").mouseleave(function (e) { $(this).children("div.over").stop(true, true).fadeOut(600) });
          
    
    $(".notas2").html(($(".notas").html()).replace('class="carousel slide"', 'class="carousel2 slide"'));

    // Bind atrr & load nota
    $(".nota").attr("href", "JavaScript:$('#myModal').modal()");
    $(".nota").attr("data-target", "#myModal");
    $(".nota").attr("data-toggle", "modal");
    $('#myModal').on('show.bs.modal', function (event) { $.ajax({ url: "notas/n" + $(event.relatedTarget).data('id') + ".html", cache: false }).done(function (html) { $(".modal-body").html(html) }) });
    
    
});


function SeccionActive(LiSel) {
    var objS = ["#menuLat li a .padres", "#menuLat li a .alumnos", "#menuLat li a .docentes", "#menuLat li a .programas"];
    "#Spad" == LiSel ? $(objS[0]).addClass("padresSel") : $(objS[0]).removeClass("padresSel");
    "#Salu" == LiSel ? $(objS[1]).addClass("alumnosSel") : $(objS[1]).removeClass("alumnosSel");
    "#Sdoc" == LiSel ? $(objS[2]).addClass("docentesSel") : $(objS[2]).removeClass("docentesSel");
    "#Spro" == LiSel ? $(objS[3]).addClass("programasSel") : $(objS[3]).removeClass("programasSel");
}