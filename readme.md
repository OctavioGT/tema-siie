# Tema SIIE #

[http://siie.tamaulipas.gob.mx/demo/](http://siie.tamaulipas.gob.mx/demo/)

## Description ##
Unofficial theme - SET | Gobierno del Estado de Tamaulipas 2016-2022

    Added features: three-level and mobile menu, scrollspy 

## Requirements ##
* Bootstrap v3.3.5
* jQuery v3.1.1
* jQuery UI v1.12.1

## Developer Documentation ##
In the Code.

## Notes ##
Files:  default.html, pagina.html

## References ##
* https://github.com/twbs/bootstrap
* https://jquery.com
* https://developer.mozilla.org/es/docs/Web/CSS

### Contact ###
lacho.gutierrez@gmail.com


P.D. Let's go play !!!




